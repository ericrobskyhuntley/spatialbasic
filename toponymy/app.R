library(shiny)
library(sortable)
library(jsonlite)
library(stringr) 
require(htmltools)
library(shinyBS)
library(shinyjs)
require(sf)
require(dplyr)
require(leaflet)


# File read-in
streets <-st_read('streets.geojson')  %>% 
  st_transform(4326) %>%
  filter(!is.na(category)) %>%
  dplyr::select(c(street_nam, category))


# Variables renaming (eric)
streets_to_tags <- function(df){
  apply(
    df,
    1,
    function(row) {
      list("test" = tags$div(
        id = row['street_nam'],
        class = row['category'],
        h3(str_to_title(row['street_nam'])),
      ))
    }
  )
}


# Colors for palette
pal <- colorFactor(
  palette = c('#FDB34E', '#FB4D42', '#974D9D','#D5A6BD', '#304D80'),
  domain = streets$category
)


# Main page setup
ui <- fillPage(
  
  # Style guide
  tags$head(
    #tags$style(HTML(".bucket-list-container {min-height: 350px;}")),
    includeCSS("styles.css")
  ),
  tags$style(type = "text/css", "html, body {width:100%; height:100%}"),
  useShinyjs(),
  # Title bar
  absolutePanel(
    style = "top: 2.5%; z-index: 3; width: 100%; text-align: center",
    fluidRow(
      column(
        width = 3,
        # How to play button.
        actionButton(
          "howto",
          style = "min-height: 80px; background-color: rgba(255,255,255, 0.8); color: #1DBCB4; border-color: transparent; border-left: 1px solid #1DBCB4; border-bottom: 1px solid #1DBCB4; border-radius: 0", 
          h3("How to Play")
        ),
        # Guiding questions button.
        actionButton(
          "guiding",
          style = "min-height: 80px; background-color: rgba(255,255,255, 0.8); color: #1DBCB4; border-color: transparent; border-left: 1px solid #1DBCB4; border-bottom: 1px solid #1DBCB4; border-radius: 0", 
          h3("Guiding Questions")
        ),
      ),
      column(
        width = 6,
        style = "min-height: 80px; background-color: rgba(255,255,255, 0.8); color: #1DBCB4; border-color: transparent; border-left: 1px solid #1DBCB4; border-bottom: 1px solid #1DBCB4; border-radius: 0",
        h1("Making Sense of Place Names"),
      )
    )
    ),
    

    # Transparent side panel on top of the map
    absolutePanel(
      left = "80%", 
      width = "20%",
      style = "z-index: 5; background-color: transparent; bottom: 0;",
      
      # Side panel for street categories
      wellPanel(
        style = "background-color: transparent; border-color: transparent",
        class = "buckets",
        bucket_list(
            header = "",
            group_name = "bucket_list_group",
            orientation = "vertical",
            add_rank_list(
              text = h4(id = "scottish","Scottish"),
              class = "scottish_list",
              labels = NULL,
              input_id = "scottish",
              options = sortable_options(
                onAdd = htmlwidgets::JS("function (evt) { 
                                        if (!(evt.item.firstElementChild.className == 'Scottish')) {
                                          evt.from.append(evt.item);
                                        }
                                        if (evt.to.lastElementChild.classList.contains('active')) {
                                          evt.item.classList.add('active');
                                        }
                                      }")
                )
              ),
            add_rank_list(
              text = h4(id = "irish", "Irish"),
              class = "irish_list",
              labels = NULL,
              input_id = "irish",
              options = sortable_options(
                onAdd = htmlwidgets::JS("function (evt) { 
                                        if (!(evt.item.firstElementChild.className == 'Irish')) {
                                          evt.from.append(evt.item);
                                        }
                                        if (evt.to.firstElementChild.classList.contains('active')) {
                                          evt.item.classList.add('active');
                                        }
                                      }")
                )
              ),
            add_rank_list(
              text = h4(id = "italian", "Italian"),
              class = "italian_list",
              labels = NULL,
              input_id = "italian",
              options = sortable_options(
                onAdd = htmlwidgets::JS("function (evt) { 
                                        if (!(evt.item.firstElementChild.className == 'Italian')) {
                                          evt.from.append(evt.item);
                                        }
                                        if (evt.to.firstElementChild.classList.contains('active')) {
                                          evt.item.classList.add('active');
                                        }
                                      }")
                )
              ),
            add_rank_list(
              text = h4(id = "indigenous", "Indigenous"),
              class = "indigenous_list",
              labels = NULL,
              input_id = "indigenous",
              options = sortable_options(
                onAdd = htmlwidgets::JS("function (evt) { 
                                        if (!(evt.item.firstElementChild.className == 'Indigenous')) {
                                          evt.from.append(evt.item);
                                        }
                                        if (evt.to.firstElementChild.classList.contains('active')) {
                                          evt.item.classList.add('active');
                                        }
                                      }")
                )
              ),
            add_rank_list(
              text = h4(id = "british", "British"),
              class = "british_list",
              labels = NULL,
              input_id = "british",
              options = sortable_options(
                onAdd = htmlwidgets::JS("function (evt) { 
                                        if (!(evt.item.firstElementChild.className == 'British')) {
                                          evt.from.append(evt.item);
                                        }
                                        if (evt.to.firstElementChild.classList.contains('active')) {
                                          evt.item.classList.add('active');
                                        }
                                      }")
                )
              )
            )
          ),
        ),

        
  # Street pop up box
  absolutePanel(
    width = "100%",
    style = "z-index: 1",
    bottom = "25%",
    class = "namelist",
    fluidRow(
      column(
        width = 4,
        offset = 4,
        uiOutput("bucketlist"),
      )
    )
  ),
  leafletOutput("streetmap",  width = "100%", height = "100%"),
  # titlePanel(h2("What can we uncover about the place names around us? What patterns do we notice?")),
)
  


# Server function handles the dynamic, interactive commands.
server <- function(input, output, session) {
  
  rv <- reactiveValues(n = 0)
  
  rv$n <- isolate(rv$n) + 1
  
  # Back end of street categories
  output$bucketlist <- renderUI({
    bucket_list(
      header = "",
      group_name = "bucket_list_group",
      orientation = "horizontal",
      add_rank_list(
        text = h4("Street Name"),
        css_id = "streetnaming",
        labels = bucket_list_label(),
        input_id = "bucketlist"
      )
    )
  })
  
  bucket_list_label <- reactive({
    streets_to_tags(streets[rv$n + 1,] %>% st_drop_geometry())
  })
  
  
  # Popup with introduction
  showModal(modalDialog(
    title = h5("This is a guessing game where you’ll try to match Boston street names with the different types of naming conventions."),
    h6("Boston was originally occupied by Indigenous peoples. Over time, many waves of European immigrants have moved to Boston. As a result, streets were given different ethnically European or Indigenous names. Let's explore where these names appear and what we can learn from those patterns.
Just remember - the origins of a name don't necessarily tell us about an individual person's ethnicity or the ethnicities of people living in the area today."),
    size = c("m"),
    easyClose = TRUE,
    footer = NULL
  ))
  
  filteredStreets <- reactive({
    list <- as.numeric(c(input$scottish, input$irish, input$british, input$indigenous, input$italian))
    s <- streets[isolate(rv$n),]
    # This conditional fixes issue where map loads with initial street.
    if (length(list) > 0) {
      rv$n <- isolate(rv$n) + 1
      s <- streets[isolate(rv$n),]
      print(s)
      s
    } else {
      s
    }
  })
  
  
  # # Tooltip for street
  # bsTooltip(id = "street_nam", title = "Street Name",
  #           placement = "left", trigger = "hover")
  
  
  # Dynamic leaflet map using renderLeaflet
  output$streetmap <- renderLeaflet({
      leaflet(
        options = leafletOptions(
          minZoom = 12, 
          maxZoom = 16,
          zoomControl = FALSE
          )
      ) %>%
      addRectangles(
        lng1=-55.941020, lat1=33.381148,
        lng2=-143.318892, lat2=48.896573,
        color= "#1DBCB4"
      )%>%
      # Basemap
      addProviderTiles(
        providers$CartoDB.Positron,
        options = providerTileOptions(noWrap = TRUE)
      ) %>%
      # Lat-long for Boston
      setView(lng = -71.07823745636378, lat = 42.349550379756316, zoom = 14)
      
  })
  
  onclick(
    "scottish", 
    runjs(
      "var items = document.querySelector('.buckets .column_1').querySelectorAll('.rank-list-item');
      items.forEach(function(item) {
        item.classList.toggle('active');
      });"
    )
  )
  
  onclick(
    "irish", 
    runjs(
      "var items = document.querySelector('.buckets .column_2').querySelectorAll('.rank-list-item');
      items.forEach(function(item) {
        item.classList.toggle('active');
      });"
    )
  )
  
  onclick(
    "italian", 
    runjs(
      "var items = document.querySelector('.buckets .column_3').querySelectorAll('.rank-list-item');
      items.forEach(function(item) {
        item.classList.toggle('active');
      });"
    )
  )
  
  onclick(
    "indigenous", 
    runjs(
      "var items = document.querySelector('.buckets .column_4').querySelectorAll('.rank-list-item');
      items.forEach(function(item) {
        item.classList.toggle('active');
      });"
    )
  )
  
  onclick(
    "british", 
    runjs(
      "var items = document.querySelector('.buckets .column_5').querySelectorAll('.rank-list-item');
      items.forEach(function(item) {
        item.classList.toggle('active');
      });"
    )
  )
   
  observeEvent(input$howto, {
    # How to play instructions
    showModal(
      modalDialog(
        title = h3("+ How to Play"),
        tags$ol(
          tags$li("When you click the START button, the name of a street will pop up in the middle of the screen."),
          tags$li("Click and drag the street name into the appropriate category at the right of the screen."),
          tags$li("If correct, the street will appear on the map in the color corresponding to the category. If the place name does not match the category, the street will not appear on the map."),
          tags$li("When you make a correct match, click on See Similar Streets to reveal 3 more streets with the same ethnic origins. Hover over a street to see its name in the bottom right box."),
          tags$li("A series of guiding questions will pop up throughout the game. Use them to reflect on the patterns you notice."),
          tags$li("Names can change! After you sort all the names, be sure to check out the list of place-names that are currently being debated or under review.")
        ),
        easyClose = TRUE,
        footer = NULL
      )
    )
  })

  observeEvent(input$guiding, {
    # Guiding questions
    showModal(
      modalDialog(
        title = h3("+ Guiding Questions"),
        tags$ol(
          tags$li("How can street names help you understand the history of a city?"),
          tags$li("Are you familiar with any of these streets? Have you ever thought about the origins of the name before?"),
          tags$li("What kinds of patterns do you notice? Are there certain types of names clustered in particular geographic areas? If so, why do you think that is?")
        ),
        easyClose = TRUE,
        footer = NULL
      )
    )
  })
    
  observe({
    # LeafletProxy references an already-drawn map.
    sel_streets <- filteredStreets()
    bbox <- st_bbox(sel_streets)
    
    leafletProxy("streetmap")  %>%
      # Remove previously drawn shapes.
      # Add a polygon layer using the selected streets.
      addPolylines(
        data = sel_streets,
        # No outline.
        stroke = TRUE,
        # Opacity
        opacity = 1,
        # Simplify geometries
        smoothFactor = 0,
        # Fully opaque.
        fillOpacity = 0,
        # Use the above color palette to shade.
        color = ~pal(category)
      ) %>%
      flyToBounds(bbox[[1]],bbox[[2]],bbox[[3]],bbox[[4]])
  })
}

shinyApp(ui = ui, server = server)
